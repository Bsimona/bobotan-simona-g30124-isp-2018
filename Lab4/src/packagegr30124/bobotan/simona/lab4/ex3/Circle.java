package packagegr30124.bobotan.simona.lab4.ex3;

public class Circle {
    private double radius;
    private String color;

    public Circle()
    {
        radius = 1.0;
        color ="red";
    }

    public Circle(double radius)
    {
        this.radius=radius;
    }

    public double getRadius()
    {
        return radius;
    }

    public double getArea() {
        return Math.PI*(this.radius)*(this.radius);
    }
}