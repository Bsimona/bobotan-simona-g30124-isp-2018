package packagegr30124.bobotan.simona.lab8.ex4;


import java.io.*;

public class CarManager {

    Car createCar(String model,int price) {
        Car car=new Car(model, price);
        return car;
    }
    void addCar(Car car,String storeRecipientName) throws IOException
    {
        ObjectOutputStream oStream=new ObjectOutputStream(new FileOutputStream(storeRecipientName));
        oStream.writeObject(car);
        System.out.println("Scris in fisier");
    }
    Car removeCar(String storeRecipientName) throws IOException,ClassNotFoundException
    {
        ObjectInputStream iStream=new ObjectInputStream(new FileInputStream(storeRecipientName));
        Car car=(Car)iStream.readObject();
        System.out.println("Citit din fisier");
        return car;
    }

}

