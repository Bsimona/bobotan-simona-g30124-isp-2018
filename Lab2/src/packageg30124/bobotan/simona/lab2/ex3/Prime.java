package packageg30124.bobotan.simona.lab2.ex3;

import java.util.Scanner;

public class Prime{
	
	static int prim(int t){
		int i,s=0;
		for(i=1; i<=100; i++)
			if(t%i==0)
				s++;
		return s;
	}
	
	public static void main(String[] args){
		
		int i,s=0;
		int a,b;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println(" a: ");
		a = in.nextInt();
		
		System.out.println("b: ");
		b = in.nextInt();
		
		in.close();
		
		for(i=a; i<=b; i++)
			if(prim(i)==2){
				System.out.printf("%d ",i);
				s++;
			}
		System.out.println("");
		System.out.println("Nr de numere prime este: "+s);
	}
	

}
