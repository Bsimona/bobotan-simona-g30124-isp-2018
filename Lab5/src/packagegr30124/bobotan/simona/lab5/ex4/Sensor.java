package packagegr30124.bobotan.simona.lab5.ex4;


public abstract class Sensor {
    private String location;
    abstract int readValue();
    public String getLocation() {
        return "Location is "+location;
    }

}
