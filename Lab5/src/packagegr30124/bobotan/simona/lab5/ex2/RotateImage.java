package packagegr30124.bobotan.simona.lab5.ex2;

/**
 * Created by Bobo on 01.04.2018.
 */
public class RotateImage implements Image{
    private String fileName;
    public RotateImage(String fileName) {
        this.fileName=fileName;
    }

    public void display() {
        System.out.println("Display rotate "+fileName);
    }
}
