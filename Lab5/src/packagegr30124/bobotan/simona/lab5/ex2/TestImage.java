package packagegr30124.bobotan.simona.lab5.ex2;

/**
 * Created by Bobo on 01.04.2018.
 */
public class TestImage {
    public static void main(String[] args) {
        RealImage rm  = new RealImage("zeu");
        rm.display();

        ProxyImage pm = new ProxyImage("rac");
        pm.display();

        RotateImage ro = new RotateImage("nota");
        ro.display();
    }

}
