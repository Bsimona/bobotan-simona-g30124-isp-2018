package packagegr30124.bobotan.simona.lab7.ex2;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class Bank {
    ArrayList<BankAccount> accounts = new ArrayList<>();


    public void addAccount(String owner, double balance) {
        BankAccount b = new BankAccount(owner, balance);
        accounts.add(b);

    }

    public void printAccounts() {
        Collections.sort(accounts);
        Iterator<BankAccount> i = accounts.iterator();
        while (i.hasNext()) {
            BankAccount accounts = i.next();
            {
                System.out.println("Contul  utilizatorului " + accounts.getOwner() + " este: " + accounts.getBalance());

            }
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        Iterator<BankAccount> i = accounts.iterator();
        while (i.hasNext()) {
            BankAccount accounts = i.next();
            if (accounts.getBalance() > minBalance && accounts.getBalance() < maxBalance)
                System.out.println("Contul  utilizatorului " + accounts.getOwner() + " este: " + accounts.getBalance());
        }

    }

    public ArrayList<BankAccount> getAllAccounts() {
        return accounts;
    }

    public void printAccounts2() {

        Iterator<BankAccount> i = accounts.iterator();
        while (i.hasNext()) {
            BankAccount account = i.next();
            System.out.println("Contul utilizatorului " + account.getOwner() + " este " + account.getBalance());
        }

    }
}




