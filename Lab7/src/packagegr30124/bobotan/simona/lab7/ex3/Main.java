package packagegr30124.bobotan.simona.lab7.ex3;

/**
 * Created by Bobo on 07.05.2018.
 */
public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Ana", 109.98);
        bank.addAccount("Andrei", 123.67);


        bank.printAccounts();
        bank.printAccounts(51,120);

        bank.getAllAccounts();
    }
}
