package packagegr30124.bobotan.simona.lab7.ex4;


public class Main {
    public static void main(String[] args) {
        Dictionary d = new Dictionary();
        Word cuv = new Word("rosu");
        Definition def = new Definition("culoare");
        Word cuv1 = new Word("barza");
        Definition def1 = new Definition("pasare");
        Word cuv2 = new Word("leu");
        Definition def2 = new Definition("animal");
        d.addWord(cuv,def);
        d.addWord(cuv1,def1);
        d.addWord(cuv2,def2);

        d.getAllDefinition();
        d.getDefinition(cuv);
        d.getAllWords();
    }
}
