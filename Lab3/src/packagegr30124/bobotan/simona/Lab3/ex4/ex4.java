package packagegr30124.bobotan.simona.lab3.ex4;


import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class ex4 {
    public static void main(String[] args)
    {

        // Set up the initial situation
        City ny= new City();// create map
        Wall w1=new Wall(ny,1,2,Direction.WEST);
        Wall w2=new Wall(ny,2,2,Direction.WEST);
        Wall w3=new Wall(ny,1,2,Direction.NORTH);
        Wall w4=new Wall(ny,1,3,Direction.NORTH);
        Wall w5=new Wall(ny,1,3,Direction.EAST);
        Wall w6=new Wall(ny,2,3,Direction.EAST);
        Wall w7=new Wall(ny,2,2,Direction.SOUTH);
        Wall w8=new Wall(ny,2,3, Direction.SOUTH);
        Robot karel = new Robot(ny,0,4,Direction.WEST);
        int i;
        for(i=0;i<3;i++)
        {
            karel.move();
        }
        karel.turnLeft();
        for(i=0;i<3;i++)
        {
            karel.move();
        }
        karel.turnLeft();
        for(i=0;i<3;i++)
        {
            karel.move();
        }
        karel.turnLeft();
        for(i=0;i<3;i++)
        {
            karel.move();
        }
        karel.turnLeft();

    }
}
