package packagegr30124.bobotan.simona.lab3.ex5;


import becker.robots.*;

public class ex5 {
    public static void main(String[] args) {
        City ny = new City();// create map
        Wall w1 = new Wall(ny, 1, 1, Direction.WEST);
        Wall w2 = new Wall(ny, 2, 1, Direction.WEST);
        Wall w3 = new Wall(ny, 1, 1, Direction.NORTH);
        Wall w4 = new Wall(ny, 1, 2, Direction.NORTH);
        Wall w5 = new Wall(ny, 1, 2, Direction.EAST);

        Wall w7 = new Wall(ny, 2, 1, Direction.SOUTH);
        Wall w8 = new Wall(ny, 1, 2, Direction.SOUTH);
        Robot karel = new Robot(ny, 1, 2, Direction.SOUTH);
        Thing s = new Thing(ny, 2, 2);
        int i;

        for (i = 0; i < 3; i++) {
            karel.turnLeft();
        }

        karel.move();

        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.pickThing();
        for (i = 0; i < 2; i++) {
            karel.turnLeft();
        }
        karel.move();
        for (i = 0; i < 3; i++) {
            karel.turnLeft();
        }
        karel.move();
        for (i = 0; i < 3; i++) {
            karel.turnLeft();
        }
        karel.move();
        for (i = 0; i < 3; i++) {
            karel.turnLeft();
        }
    }
}
