package packagegr30124.bobotan.simona.lab6.ex3;


public class Ex4 implements CharSequence {
    private char[] sir;
    public Ex4(char[] sir) {
        // TODO Auto-generated constructor stub
        this.sir=sir;
    }

    @Override
    public char charAt(int i) {
        // TODO Auto-generated method stub
        if(i>-1 && i<sir.length)
            return sir[i];
        return 0;
    }

    @Override
    public int length() {
        // TODO Auto-generated method stub
        return sir.length;
    }

    @Override
    public String toString() {
        return new String(sir);
    }

    @Override
    public CharSequence subSequence(int i, int j) {
        // TODO Auto-generated method stub
        char[] subsir=new char[j-j+1];
        int x=0;
        for(int y=i;y<=j;y++)
            subsir[x++]=sir[y];
        return new Ex4(subsir);

    }

}
