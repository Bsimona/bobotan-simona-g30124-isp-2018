package packagegr30124.bobotan.simona.lab6.ex3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestEx4 {
    @Test
    public void test() {
        Ex4 sir = new Ex4(new char[]{'r','y','m','p','2','5','V','Z'});
        assertEquals('5',sir.charAt(5));
        assertEquals(8,sir.length());
        assertEquals("rymp25VZ",sir.toString());
        Ex4 sir2 = new Ex4(new char[]{'W','O','N','e','t','a','p','1','2'});
        assertEquals("Ne",sir2.subSequence(3,4));
    }
}
