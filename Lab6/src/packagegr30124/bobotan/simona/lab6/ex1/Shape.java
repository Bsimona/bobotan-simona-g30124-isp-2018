package packagegr30124.bobotan.simona.lab6.ex1;

/**
 * Created by Bobo on 03.04.2018.
 */
import java.awt.*;

public abstract class Shape {
    private Color color;
    private int x;
    private int y;
    private String Id;
    private boolean fill;

    public boolean isFill() {
        return fill;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }

    public Shape(Color color, int x, int y, String id, boolean fill) {
        this.color = color;
        this.x = x;
        this.y = y;

        Id = id;
        this.fill = fill;

    }

    public void SetX(int x){
        this.x =x;
    }
    public int GetX(){
        return x;
    }

    public void SetY(int y){
        this.y =y;
    }
    public int GetY(){
        return y;
    }

    public String getId() {
        return Id;
    }

    public Shape(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }


    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);



    ///public void deleteById(){}

}
