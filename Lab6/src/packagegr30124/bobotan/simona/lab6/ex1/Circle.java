package packagegr30124.bobotan.simona.lab6.ex1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int x, int y, String id, boolean fill, int radius) {
        super(color, x, y, id, fill);
        this.radius = radius;
    }


    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle " + this.radius + " " + getColor().toString());
        g.setColor(getColor());
        if (isFill() == false) {
            g.drawOval(getX(), getY(), radius, radius);
        } else
            g.fillOval(getX(), getY(), radius, radius);
    }
    }


